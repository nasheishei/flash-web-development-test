from flask import Flask

# 创建Flask类对象
app = Flask(__name__)  #
# Web服务器把 请求 发送给Flask应用实例
# 应用实例需要知道每个URL的请求要运行哪些代码
# 所以保存了一个URL到Python函数的映射关系
# 处理URL和函数之间关系的程序称为路由

# Flask中定义路由，使用app.route装饰器
@app.route('/')
def index():
    return '<h1>热烈庆祝大神基地从此成立啦！！</h>'

# 装饰器是Python语言的标准特性
# 可以把函数注册为事件处理程序，在事件发生时调用
# 这里事件就是你在浏览器里面输入地址，程序就是这个index()函数

# 编译器中运行
if __name__ == '__main__':
    app.run()
